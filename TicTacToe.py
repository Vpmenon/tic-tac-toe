import random
history = dict()
print('Tic Tac Toe !!')
count = []
values = ['']*10
def choice():
    p = int(input("Enter your choice (1-9): "))
    return p-1
def display( value ):     # To display the board
    print(value[0]+'  |  '+value[1]+'  |  '+value[2])
    print('---------------')
    print(value[3]+'  |  '+value[4]+'  |  '+value[5])
    print('---------------')
    print(value[6]+'  |  '+value[7]+'  |  '+value[8])
def mark(p, m):  # To mark the moves
    value[p] = 'X'
    value[m] = 'O'
    display(value)
def block(p,m):
    values[p] = 'X'
    values[m] = 'O'
    display(values)
def check():
    winner = None
    if (value[0] == value[1] == value[2] == 'X') or (value[3] == value[4] == value[5] == 'X') or (value[6] == value[7] == value[8] == 'X') or (value[0] == value[4] == value[8] == 'X') or (value[2] == value[4] == value[6] == 'X') or (value[2] == value[5] == value[8] == 'X') or (value[1] == value[4] == value[7] == 'X') or (value[0] == value[3] == value[6] == 'X'): winner = 'player'
    elif (value[0] == value[1] == value[2] == 'O') or (value[3] == value[4] == value[5] == 'O') or (value[6] == value[7] == value[8] == 'O') or (value[0] == value[4] == value[8] == 'O') or (value[2] == value[4] == value[6] == 'O') or (value[2] == value[5] == value[8] == 'O') or (value[1] == value[4] == value[7] == 'O') or (value[0] == value[3] == value[6] == 'O'): winner = 'computer'
    return winner
def space(value, pos):
        return value[pos] == ' '
def full( value ):
    for i in range(1, 10):
        if space(value,i):return False
    return True
for i in range (1,10):
    value = [' '] * 10
    pos = dict()
    r = 0
    count = 0
    while count<=9:
        f = 0
        p = choice()
        c = random.randint(0,8)
        if value[c] ==' ' and p != c:
            mark(p,c)
            count+=1
        else:
            if full(value):
                f = 1
                break
            else: continue
        w = check()
        pos[r] = (p, c, w)
        r += 1
        if w != None: break
    history[i] = (pos)
    
a = int(input('enter your choice'))
for j in range(0, len(pos)):
    h,c,w = history[a][j]
    block(h,c)
    print('\n----------------------------------\n')
if(w != 'draw'):
    print( w + " won the game")
else: print('The game is draw')