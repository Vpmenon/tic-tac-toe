# Tic Tac Toe

Create a Tic Tac Toe game.

Here are the requirements:

2 players should be able to play the game (user, computer)
The board should be printed out every time a player makes a move
You should be able to accept input of the player position and then place a symbol on the board
- Store the Game logs (info about the moves, result) for 10 games into a data structure.
- You should be able to recreate the moves and print out the board for each of the moves for any of the 10 games when the user inputs the game number.
For example - if the user says that he wants to see how Game 3 was played, you should print out all the moves on the board and print out the winner at the end of the game.